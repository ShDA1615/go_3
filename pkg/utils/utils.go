package utils

import (
	//"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"regexp"
)

func ShearchStrFromHTML(str string, html []string) []string {
	var r []string
	// for i := 0; i < len(html); i++ {
	// 	_, err := url.Parse(html[i])
	// 	if err == nil {
	// 		resp, err := http.Get(html[i])

	// 		if err == nil {
	// 			defer resp.Body.Close()
	// 			b, err := ioutil.ReadAll(resp.Body)
	// 			if err == nil {
	// 				matched, _ := regexp.MatchString(str, string(b))
	// 				if matched {
	// 					r = append(r, html[i])
	// 				}
	// 			}
	// 		}
	// 	}
	// }

	for _, v := range html {
		_, err := url.Parse(v)
		if err != nil {
			log.Printf("URL %v не действителен", v)
			continue
		}
		resp, err := http.Get(v)
		if err != nil {
			log.Printf("URL %v не действителен", v)
			continue
		}
		b, err := ioutil.ReadAll(resp.Body)
		resp.Body.Close()
		if err != nil {
			log.Printf("URL %v не действителен", v)
			continue
		}
		matched, _ := regexp.MatchString(str, string(b))
		if matched {
			r = append(r, v)
		}

	}
	return r
}
