package tests

import (
	"gitlab.com/ShDA1615/go_3/pkg/utils"
	"testing"
)

type TestUrl struct {
	num      int
	s        string
	urls     []string
	Expected bool
}

var testUrl = []TestUrl{
	{
		num: 1,
		s:   "golang",
		urls: []string{
			"https://golangify.com/arrayetyuwsrtj",
			"https://eax.me/golang-unit-testing/",
			"https://github.com/chibisov/go-yadisk",
			"https://itproger.com/test/golang",
		},
		Expected: true,
	},
	{
		num: 2,
		s:   "a;fsh",
		urls: []string{
			"https://golangify.com/arrayaafhadfh",
			"https://eax.me/golang-unit-testing/",
			"https://github.com/chibisov/go-yadisk",
			"https://itproger.com/test/golang",
		},
		Expected: false,
	},
}

func TestShearchStrFromHTML(t *testing.T) {
	for _, tUrl := range testUrl {
		result := utils.ShearchStrFromHTML(tUrl.s, tUrl.urls)
		if (len(result) > 0) != tUrl.Expected {
			t.Errorf("Номер теста: %d  ожидаемая величина: %v  результат: %v",
				tUrl.num,
				tUrl.Expected,
				result)
		}
	}
}
